#@link: https://github.com/hashicorp/terraform/blob/master/examples/aws-two-tier/main.tf

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "default" {
  name        = "video_processing_secutity_group"
  description = "video_processing_secutity_group to allow 80, 22, 443 ports"
  vpc_id      = "${var.vpc_id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "video-processing" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"
  availability_zone = "${var.availability_zone}"

  connection {
    user = "${var.user}"
    private_key = "${file("${var.private_key_path}")}"
  }

  key_name = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.default.id}", "${var.app_security_group_id}"]

  tags {
    Name = "prod-video-processing"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo service supervisor restart",
    ]
  }

}