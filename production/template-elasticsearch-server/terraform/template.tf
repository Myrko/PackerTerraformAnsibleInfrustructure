#@link: https://github.com/hashicorp/terraform/blob/master/examples/aws-two-tier/main.tf
#@link: https://github.com/nadnerb/terraform-elasticsearch/blob/master/main.tf

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_security_group" "elasticsearch" {
  name = "elasticsearch-security-group"
  description = "elasticsearch ports with ssh"
  vpc_id = "${var.vpc_id}"

  # SSH access from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 9200
    to_port = 9400
    protocol = "tcp"
    cidr_blocks = ["${var.ingress_cidr_blocks_for_es_ports}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "elasticsearch"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "elasticsearch-server" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"
  associate_public_ip_address = false
  ebs_optimized = false
  availability_zone = "${var.availability_zone}"
  vpc_security_group_ids = ["${aws_security_group.elasticsearch.id}"]
  key_name = "${var.key_name}"

  lifecycle {
    create_before_destroy = true
  }

  connection {
    user = "${var.user}"
    private_key = "${file("${var.private_key_path}")}"
  }

  root_block_device {
    volume_type = "${var.root_volume_type}"
    volume_size = "${var.root_volume_size}"
    delete_on_termination = "true"
  }

  tags {
    Name = "prod-elasticsearch-server"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo service elasticsearch restart",
    ]
  }
}