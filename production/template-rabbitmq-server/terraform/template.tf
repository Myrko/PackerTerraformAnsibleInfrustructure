#@link: https://github.com/hashicorp/terraform/blob/master/examples/aws-two-tier/main.tf

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "default" {
  name        = "rabbitmq_default_security_group"
  description = "rabbitmq default security group to allow 80, 22, 5672, 15672 ports"
  vpc_id      = "${var.vpc_id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 5672
    to_port     = 5672
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 15672
    to_port     = 15673
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "rabbitmq" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"
  availability_zone = "${var.availability_zone}"

  connection {
    user = "${var.user}"
    private_key = "${file("${var.private_key_path}")}"
  }

  key_name = "PNappServerKeyPair"
  vpc_security_group_ids = ["${aws_security_group.default.id}", "${var.app_security_group_id}"]

  tags {
    Name = "prod-rabbitmq"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo hostname localhost",
      "sudo service rabbitmq-server restart",
    ]
  }
}