variable "access_key" {}
variable "secret_key" {}
variable "ami" {}
variable "instance_type" {}
variable "key_name" {}
variable "public_key_path" {}
variable "private_key_path" {}
variable "app_security_group_id" {}
variable "vpc_id" {}
variable "availability_zone" {
  default = "eu-central-1b"
}
variable "region" {
  default = "eu-central-1"
}
variable "user" {
  default = "ubuntu"
}