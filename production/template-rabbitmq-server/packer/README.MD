To launch packer use:

        packer build \
            -var 'aws_access_key=foo' \
            -var 'aws_secret_key=bar' \
            template.json
            
Or copy variables.dist.json into variables.json and execute it from shell as follows:

        packer build -var-file=variables.json template.json