
1. cd into production/template-rabbitmq-server

2. navigate to ansible/roles/rabbitmq/vars/main.yml
3. setup variables as you need
4. go back to production/template-rabbitmq-server
5. navigate to packer/
6. open template.json and setup aws credentials and desired values for each key you have
7. run: packer build template.json
8. after successfull build there will be displayed your ami id, you can also find it in web in aws ec2 menu called AMI`s
9. remember ami id (previous image can be deleted, if you have such desire, usually i do this)
10. go to production/template-rabbitmq-server/terraform
11. edit terraform.tfvars, paste you amiid from previous step
12. run: terraform plan (it will show what it is going to do)
13. run: terraform apply (will apply all changes and craete instance from your ami)
14. after you run terraform apply you will have two file with instance state. They should be available globally, but for now, i just use to run terraform apply and after delete file of state. 
15. @todo create a flow to work with instance state (save state on S3, so no matter where you manage an instance)
16. done