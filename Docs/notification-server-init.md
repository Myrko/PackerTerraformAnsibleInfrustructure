
notification-server:

1. cd into production/template-notification-server
2. cd into ansible
3. open production/template-notification-server/ansible/roles/notification-server/vars/main.yml
4. set repo user and password (can be found in Template wiki)
5. navigate to production/template-notification-server/ansible/roles/nginx/files
6. copy nginx websocket-proxy.conf.dist config into websocket-proxy.conf and pass desired variables
7. navigate to packer/
8. open template.json and setup aws credentials and desired values for each key you have
9. run: packer build template.json
10. after successfull build there will be displayed your ami id, you can also find it in web in aws ec2 menu called AMI`s
11. remember ami id (previous image can be deleted, if you have such desire, usually i do this)
12. go to production/template-notification-server/terraform
13. edit terraform.tfvars, paste you amiid from previous step
14. run: terraform plan (it will show what it is going to do)
15. run: terraform apply (will apply all changes and craete instance from your ami)
16. after you run terraform apply you will have two file with instance state. They should be available globally, but for now, i just use to run terraform apply and after delete file of state. 
17. @todo create a flow to work with instance state (save state on S3, so no matter where you manage an instance)
18. done