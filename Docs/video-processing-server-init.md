

template-video-processing-server:


1. go to production/template-video-processing-server/ansible/roles/nginx/files
2. copy nginx video-processing-server.conf.dist config into video-processing-server.conf and pass desired variables
3. go to production/template-video-processing-server/ansible/roles/php/files and configure php.ini as you desire
4. adjust production/template-video-processing-server/ansible/roles/supervisor/files/video-processing-consumer.conf if needed
5. configure symfony config and parameters.yml files in production/template-video-processing-server/ansible/roles/video_processing/files
6. set ips of rabbitmq and notification server
7. enter production/template-video-processing-server/ansible/roles/video_processing/vars
8. copy nginx main.yml.dist config into main.yml.conf and pass desired variables (Prestigenew wiki)
9. navigate to packer/
10. open template.json and setup aws credentials and desired values for each key you have
11. run: packer build template.json
12. after successfull build there will be displayed your ami id, you can also find it in web in aws ec2 menu called AMI`s
13. remember ami id (previous image can be deleted, if you have such desire, usually i do this)
14. go to production/template-rabbitmq-server/terraform
15. edit terraform.tfvars, paste you amiid from previous step
16. run: terraform plan (it will show what it is going to do)
17. run: terraform apply (will apply all changes and craete instance from your ami)
18. after you run terraform apply you will have two file with instance state. They should be available globally, but for now, i just use to run terraform apply and after delete file of state. 
19. @todo create a flow to work with instance state (save state on S3, so no matter where you manage an instance)
20. done