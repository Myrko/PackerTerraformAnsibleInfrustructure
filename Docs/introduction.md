**Guide to deployment via Ansible, Packer, Terraform**

Introduction:

Infrastructure contains of environments. As for now there are 4 environments: 

1. Local
2. Testing (prstgnws.com)
3. Release-candidate (rc.prstgnws.com)
4. Production (template.com)

As for now, there is not any infrastructure for local development (@todo create sharable local infrastructure). Other environments consist from different amount of steps.

Testing environment consist of following structure:

	testing:
		- ansible
		- packer
		- terraform

Ansible is an open-source automation engine that automates software provisioning, configuration management, and application deployment. Link: https://www.ansible.com
Ansible uses playbook files with yml extension. In playbook you discover wich server steps should be done on a machine. 

Example: 
```yaml

  - name: Example task
      hosts: localhost
      tasks:
        - command: /bin/bash echo "hello"
```
		

Since we have a lot of stuff to install we use suggested scheme for Ansible called "Roles". Basic structure (directories that are not required can be skipped):

		site.yml
		webservers.yml
		fooservers.yml
		roles/
		   php/
		     files/
		     templates/
		     tasks/
		     handlers/
		     vars/
		     defaults/
		     meta/

Roles consist of initial file called site.yml:

		- name: Setup example role on instance 
		  hosts: localhost 
		  roles: 
		  	- php  


And concrete role (directories that are not required can be skipped):

		roles/
		   php/
		     files/
		     templates/
		     tasks/
		     handlers/
		     vars/
		     defaults/
		     meta/

Main executable file is in tasks directory. It is called main.yml.


		roles/
		   php/
		     files/
		     tasks/
		     	/main.yml

One of the most important directory is variables directory. It consist of list of variables adn their values that are env specific (rabbitmq variables example):

		 	application_virtual_host: template
			application_user: template
			application_user_password: template
			application_exchange_web: send-web-push
			application_queue_web: send-web-push
			application_exchange_convertation_video: convertation-video
			application_queue_convertation_video: convertation-video


It consist of list of the tasks that should be executed (followin taks is going to setup PHP7.0 and its dependencies, install composer and copy php.ini that lies in directory files to desired sestination):

		---

		- name: Install php7.0 and dependency packages
		  apt: name={{ item }} allow_unauthenticated=yes
		  with_items:
		      - curl
		      - php7.0
		      - php7.0-cli
		      - php7.0-common
		      - php7.0-cgi
		      - php7.0-curl
		      - php7.0-imap
		      - php7.0-pgsql
		      - php7.0-sqlite3
		      - php7.0-mysql
		      - php7.0-fpm
		      - php7.0-intl
		      - php7.0-gd
		      - php7.0-json
		      - php-memcached
		      - php-memcache
		      - php-imagick
		      - php7.0-xml
		      - php7.0-mbstring
		      - php7.0-ctype
		      - php7.0-dev
		      - php-pear
		      - php7.0-bcmath
		      - git

		- name: Install composer
		  shell: curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

		- name: Copy pre-configured php.ini
		  copy:
		      src: php.ini
		      dest: /etc/php/7.0/fpm/php.ini


In case of test server we have following roles:

		    - elasticsearch
		    - ffmpeg
		    - nodejs
		    - mysql
		    - php
		    - nginx
		    - gulp
		    - www-prestige-notification
		    - www-prestige-rabbitmq
		    - www-prestige-video-processing
		    - www-prestige-application
		    - supervisor

To run this described roles (on local, in manual mode) you need to install ansible and run following:

			/bin/bash ansible-playbook site.yml 

2. Packer is a tool for creating machine and container images for multiple platforms from a single source configuration. Link: https://www.packer.io

	packer configuration is a simple json file. It has required(mandatory fileds):

	- "variables" - your env specific variables required for some specific cases like:
		
		    "access_key" : "{{ `access_key` }}"

	- "builders" - are agents that are going to build your image. Image can be AWS AMI, DOcker image, Vagrant image, Digital ocean droplet or so ...

	- "provisioners" - list of agents that will configure your instance, example:

            {
              "type": "shell",
              "inline": [
                "sudo apt-get update",
                "sudo apt-get -y install ansible",
              ]
            },


In our case we use variables:
		
            "variables": {
               "aws_access_key": "",  - amazon access key for console execution
               "aws_secret_key": ""   - amazon secrete key
            },

Builder Amazon:

		"builders": [
		    {
		      "type": "amazon-ebs",
		      "access_key": "{{user `aws_access_key`}}",
		      "secret_key": "{{user `aws_secret_key`}}",
		      "region": "eu-central-1",
		      "source_ami": "ami-8504fdea",
		      "instance_type": "t2.small", //it is a builder, not an instance that is going to be running
		      "ssh_username": "ubuntu",
		      "ami_name": "prod-application-server {{timestamp}}"
		    }
	    ],

Provisioners (shell and ansible):

	"provisioners": [
	    {
	      "type": "shell",
	      "inline": [
	        "sleep 10",
	        "sudo apt-get update",
	        "sudo apt-get -y install software-properties-common",
	        "sudo apt-add-repository ppa:ansible/ansible",
	        "sudo apt-get update",
	        "sudo apt-get -y install ansible",
	        "sudo apt-get -y install python-pip",
	        "sudo pip install requests"
	      ]
	    },
	    {
	      "type": "ansible-local",
	      "playbook_file": "../ansible/site.yml",
	      "role_paths": [
	        "../ansible/roles/nodejs",
	        "../ansible/roles/gulp",
	        "../ansible/roles/php",
	        "../ansible/roles/nginx",
	        "../ansible/roles/www-prestige-application"
	      ]
	    }
	  ]

To run packer builder, first install packer than run:

	 packer build template.json

 You will see in command promt how packer creates a builder, which provisioners are running and which concrete steps are currently  are executed.
 After packer finsih its job, you will see an image id (in case of amazon ami-cjk234), also you can find it in aws ec2 menu called ami. Aws amis are private, 
 so noone else can see them. These images can be also used for manual cration of instance. When you choose new instance, select menu "My AMI`s" and proceed as usual.
 After image was created we use next tool called terraform.


3. Terraform - Terraform enables you to safely and predictably create, change, and improve production infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned. link: https://terraform.io

    **Tooltip there is a plugin for PHP-storm for terraform files called:** `HCL language support`
   terraform consist of following structure:

		template.tf - main file with description of which resources should be created/update/deleted
		terraform.tfvars - your variables for building instance
		terraform.tfvars.dist - example of variables
		variables.tf - variables announce, their name, description and default values


main file consist of (in our case):

		- provider
		- resource "terraform_command_name" "your-specific-resource-name"

RC template.tf explanation:		
```hcl-terraform


    provider "aws" {
      access_key = "${var.access_key}" -- aws access key
      secret_key = "${var.secret_key}" -- aws sectret key
      region     = "${var.region}"  -- desired region (setups in terraform.tfvars)
    }

    resource "aws_security_group" "rc_security_group" { -- api call to create/update/delete security group named  "rc_security_group" 
      name = "rc-security-group" -- name of security group
      description = "rc security group" -- description
      vpc_id = "${var.vpc_id}" -- in which vpc should be created

      # SSH access from anywhere
      ingress {  -- all trafic accepted  ports/protocol/subnet
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }

      ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }

      ingress {
        from_port = 15672
        to_port = 15672
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }

      # outbound internet access
      egress {  -- all trafic exported  ports/protocol/subnet
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
      }

      tags {
        Name = "rc security group" -- tag
      }

      lifecycle {
        create_before_destroy = true -- lfiecycle event
      }
    }

    resource "aws_instance" "rc-application-server" { -- main part, aws instance creation, name rc-application-server
      ami           = "${var.ami}" -- from which ami should be created (here we should paste our ami that was created by packer)
      instance_type = "${var.instance_type}" -- desired instance type
      ebs_optimized = false  -- should it be ebs optimized
      availability_zone = "${var.availability_zone}" -- which availability zone to use
      vpc_security_group_ids = ["${aws_security_group.rc_security_group.id}"] -- our previous resource (security group) id. Btw, if u want to create already existing it fails, if delete resource that is in use it also fails.

      key_name = "${var.key_name}" -- name of ssh key (check variables file)

      lifecycle {
        create_before_destroy = true
      }

      connection {
        user = "${var.user}" -- name of user
        private_key = "${file("${var.private_key_path}")}" -- path to ssh key
      }

      root_block_device { -- your deired volue root
        volume_type = "${var.root_volume_type}"  -- type
        volume_size = "${var.root_volume_size}"  -- size
        delete_on_termination = "true" -- should it be deleted when you destroy instance
      }

      tags {
        Name = "rc-application-server"
      }

      provisioner "remote-exec" {  --after creation of instnce should be lauched tasks:
        inline = [
          "sudo sevice php7.0-fpm restart",
          "sudo service nginx restart",
          "sudo service elasticsearch restart"
        ]
      }
    }
```

More on how to work with this tools you can find in official sites and guidelines.


**Important: i do not delete instance state created by terraform jsut as it generated. usually i store it till a moment when i need to rebuild instance. I run terraform destroy and it destroys everything it has created. Just after than i delete state. BY original flow it should not be deleted, terraform will handle state changes. But since we can use it seperately (i.e no shared state between our  machine) i usually delete it. Consider to add posibility to store it in s3.**
