application server:

1. go to production/template-application-server/ansible/roles/nginx/files
2. copy nginx template.conf.dist config into template.conf and pass desired variables 
3. go to production/template-application-server/ansible/roles/php/files
4. edit  php.ini as you desire
5. go to production/template-application-server/ansible/roles/supervisor/files
6. edit report supervisor configuratio if needed
7. go to production/template-application-server/ansible/roles/www-prestige-application/files
8. edit symfony configs and parameters.yml as you need. Paste ips or domain names from  rabbit/notification/video-processing servers (if you rebuilded them they will change ip)
9. @todo assign static ip to rabbit/notification/video-processing servers for not changing them in parameter.yml every time
10. production/template-application-server/ansible/roles/www-prestige-application/vars
11. copy nginx main.yml.dist config into main.yml and pass desired variables (Template wiki)
12. navigate to packer/
13. open template.json and setup aws credentials and desired values for each key you have
14. run: packer build template.json
15. after successfull build there will be displayed your ami id, you can also find it in web in aws ec2 menu called AMI`s
16. remember ami id (previous image can be deleted, if you have such desire, usually i do this)
17. go to production/template-rabbitmq-server/terraform
18. edit terraform.tfvars, paste you amiid from previous step
19. run: terraform plan (it will show what it is going to do)
20. run: terraform apply (will apply all changes and craete instance from your ami)
21. after you run terraform apply you will have two file with instance state. They should be available globally, but for now, i just use to run terraform apply and after delete file of state. 
22. @todo create a flow to work with instance state (save state on S3, so no matter where you manage an instance)
23. done 
