#@link: https://github.com/hashicorp/terraform/blob/master/examples/aws-two-tier/main.tf
#@link: https://github.com/nadnerb/terraform-elasticsearch/blob/master/main.tf

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_security_group" "testing_security_group" {
  name = "testing-security-group"
  description = "testing security group"
  vpc_id = "${var.vpc_id}"

  # SSH access from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 15672
    to_port = 15672
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "testing security group"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "elasticsearch-server" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"
  ebs_optimized = false
  availability_zone = "${var.availability_zone}"
  vpc_security_group_ids = ["${aws_security_group.testing_security_group.id}"]
  key_name = "${var.key_name}"

  lifecycle {
    create_before_destroy = true
  }

  connection {
    user = "${var.user}"
    private_key = "${file("${var.private_key_path}")}"
  }

  root_block_device {
    volume_type = "${var.root_volume_type}"
    volume_size = "${var.root_volume_size}"
    delete_on_termination = "true"
  }

  tags {
    Name = "test-application-server"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo sevice php7.0-fpm restart",
      "sudo service nginx restart"
    ]
  }
}