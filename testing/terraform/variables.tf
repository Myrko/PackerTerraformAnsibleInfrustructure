variable "access_key" {}
variable "secret_key" {}
variable "ami" {}
variable "instance_type" {
  default = "t2.medium"
}
variable "key_name" {}
variable "public_key_path" {}
variable "private_key_path" {}
variable "vpc_id" {}
variable "availability_zone" {
  default = "eu-central-1b"
}
variable "region" {
  default = "eu-central-1"
}
variable "user" {
  default = "ubuntu"
}

variable "root_volume_type" {
  default = "gp2"
}
variable "root_volume_size" {
  default = "20"
}

variable "ebs_volume_name" {
  default = "/dev/sdh"
}
variable "ebs_volume_size" {
  default = "10"
}
variable "ebs_volume_encryption" {
  default = "false"
}

variable "ingress_cidr_blocks_for_es_ports" {
  default = "0.0.0.0/0"
}